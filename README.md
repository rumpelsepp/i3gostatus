# i3gostatus

[![GoDoc](https://godoc.org/github.com/rumpelsepp/i3gostatus?status.svg)](https://godoc.org/github.com/rumpelsepp/i3gostatus)
[![Build Status](https://travis-ci.org/rumpelsepp/i3gostatus.svg?branch=master)](https://travis-ci.org/rumpelsepp/i3gostatus)
[![codecov](https://codecov.io/gh/rumpelsepp/i3gostatus/branch/master/graph/badge.svg)](https://codecov.io/gh/rumpelsepp/i3gostatus)
[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/510/badge)](https://bestpractices.coreinfrastructure.org/projects/510)
